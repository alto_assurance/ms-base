FROM python:3.7-alpine3.9

#build args
ARG BRANCH_UTIL_LIB=master
ARG BRANCH_UTIL_MODEL=master

RUN apk update \
    && apk add --no-cache --virtual .build-deps gcc libc-dev libxslt-dev \
    && apk add --no-cache libxslt \
    && apk add --update build-base git make \
    && apk add libffi-dev openssh tzdata mysql-dev \
    && pip install --upgrade pip \
    && pip install gunicorn==19.9.0

WORKDIR /app-run
COPY . /app-run

RUN pip install --no-cache-dir -r /app-run/requirements.txt

RUN ash /app-run/sh/install_libs.sh

RUN apk del .build-deps


ENTRYPOINT gunicorn --conf /app-run/server_conf.py server:app --reload
