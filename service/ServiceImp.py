from alto.lib.database.DBConnection  import DBConnection
from alto.lib.database.mongodb.UtilMongo  import UtilMongo

from alto.lib.exceptions.ExceptionApi import ExceptionApi
from alto.lib.log.UtilLog import UtilLog

from service.database.custos.User import User
from service.database.mongo_assurance.Task import Task
from alto.lib.database.models.mongodb.assurance.Route import Route

import time
import json
import mongoengine

class QuerySetEncoder(json.JSONEncoder):
	def default(self, obj):
		if isinstance(obj, mongoengine.queryset.QuerySet):
			return [obj.real, obj.imag]
		# Let the base class default method raise the TypeError
		return json.JSONEncoder.default(self, obj)


from alto.lib.service.ServiceSignals import ServiceSignals
from alto.lib.service.ServiceEventSocket import ServiceEventSocket
class ServiceImp:
	
	@staticmethod
	def sendSignal(data):
		return ServiceSignals.sendSignal(data)
	@staticmethod
	def newSocketEvent(event):
		return ServiceEventSocket.sendEvent(event)
	@staticmethod
	def testRoute(param):
		UtilLog.debug(param)
		items = UtilMongo.list(Route,param)
		return items

	@staticmethod
	def testTask():
		items = Task.objects[0:5].to_json(indent=2)
		UtilLog.debug('result:')
		UtilLog.debug(items)
		return items
	@staticmethod
	def testMongo(coll):
		ov = DBConnection.getMongoCollection(coll)
		return ov.find({},{'_id':0}).limit(10)
	
	@staticmethod
	def list_users():
	    query = User.query
	    return [x.to_json(as_dict=True) for x in query.slice(0, 50)]
	@staticmethod
	def valid_int(int_value):
		try:
			v = int(int_value)
			return "%s is int"%v
		except Exception as e:
			raise ExceptionApi(400,'se requiere valor sea entero')
	
	@staticmethod
	def stream_data():
		for i in range(0,10):
			x= 'step:%s\n\n'%(i+1)
			UtilLog.debug(x)
			yield x
			time.sleep(5)


	@staticmethod
	def file_upload(files):
		UtilLog.debug(files)
		raise ExceptionApi(500,'File upload not implemented yet !!!')

	@staticmethod
	def get_user(id_user):
		return { 'name':'Rodrigo Bernal', 'age':25}


	