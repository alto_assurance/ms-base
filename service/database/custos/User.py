from service.database.custos import db

class User(db.Model):
    __tablename__ = 'person'

    id = db.Column('idaltop',
                   db.Integer,
                   primary_key=True)
    email = db.Column('email',
                      db.String(320),
                      nullable=True)
    username = db.Column('username',
                         db.String(50),
                         nullable=True)

    def to_json(self, as_dict=False):
        output = {
            'id': self.id,
            'email': self.email,
            'username': self.username,
        }
        if as_dict:
            return output
        return json.dumps(output)

    @classmethod
    def get_by_id(cls, user_id):
        item = cls.query \
                  .filter_by(id=user_id) \
                  .filter_by(status=1) \
                  .first()
        return item