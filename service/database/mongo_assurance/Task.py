from service.database.mongo_assurance import dbm
from mongoengine_goodjson import Document, EmbeddedDocument

class Task(Document):
    # todo: Implement indexes
    PENDING = 'pending'
    IN_PROGRESS = 'in_progress'
    SUCCESS = 'success'
    ERROR = 'error'
    STATUS_CHOICES = (PENDING, IN_PROGRESS, SUCCESS, ERROR)

    HIGH = ' high'
    MEDIUM = 'medium'
    LOW = 'low'
    PRIORITY_CHOICES = (HIGH, MEDIUM, LOW)

    # Misc fields
    task_id = dbm.StringField(unique=True)
    title = dbm.StringField(required=True)
    status = dbm.StringField(choices=STATUS_CHOICES, default=PENDING)
    priority = dbm.StringField(choices=PRIORITY_CHOICES)
    #history = dbm.EmbeddedDocumentListField(TaskHistoryEntry, default=None)
    

    meta = {
        'collection': "tasks"
    }