from service.ServiceImp import ServiceImp

from flask import request,Response
from bson.json_util import dumps
import json

class ServiceFacade:
	KEY_VALUES = 'ms.statics.values'


	@staticmethod
	def sendSignal():
		res = ServiceImp.sendSignal(request.json)
		return Response(
					response=json.dumps(res),
					status=200, 
					mimetype="application/json"
		)
	@staticmethod
	def newSocketEvent():
		res = ServiceImp.newSocketEvent(request.json)
		return Response(
					response=json.dumps(res),
					status=200, 
					mimetype="application/json"
		)
	@staticmethod
	def testRoute():
		res = ServiceImp.testRoute(json.loads( request.args.get('param') ))
		return Response(
					response=json.dumps(res),
					status=200, 
					mimetype="application/json"
		)
	@staticmethod
	def testTask():
		res = ServiceImp.testTask()
		return Response(
					response=res,
					status=200, 
					mimetype="application/json"
		)
	@staticmethod
	def testMongo(collName):
		res = ServiceImp.testMongo(collName)

		return Response(
					response=dumps(res),
					status=200, 
					mimetype="application/json"
		)

	@staticmethod
	def list_users():
		l = ServiceImp.list_users()
		return Response(
					response=json.dumps(l),
					status=200, 
					mimetype="application/json"
		)
	@staticmethod
	def valid_int(int_value):
		l = ServiceImp.valid_int(int_value)
		return Response(
					response=json.dumps(l),
					status=200, 
					mimetype="application/json"
		)
	@staticmethod
	def stream_data():
		return Response(
					response=ServiceImp.stream_data(),
					status=200, 
					mimetype="text/event-stream"
		)
	@staticmethod
	def file_upload():
		return Response(
					response=ServiceImp.file_upload(request.files),
					status=200, 
					mimetype="text/event-stream"
		)

	@staticmethod
	def get_user(id_user):
		return Response(
					response= json.dumps( ServiceImp.get_user(id_user)),
					status=200, 
					mimetype="application/json"
		)