openapi: "3.0.0"
info:
  title: Service Base
  version: "1.0.0"
  description: This is a base service used for python based microservices with several connection examples 
servers:
  - url: https://development.altotrack.com/base/api/v1
  - url: http://demo.altotrack.com/base/api/v1
  - url: http://local.altotrack.com:8010/base/api/v1
  - url: http://localhost:8100/base/api/v1

# Paths supported by the server application
paths:
  /signal:
    post:
      operationId: "service.ServiceFacade.ServiceFacade.sendSignal"
      tags:
      - "gps"
      summary: send signal 
      description: "New SignalRequest"
      requestBody:
        description: SignalRequest body
        required: true
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/SignalRequest'
      responses:
        200:
          description: "Action creted Success"
        400:
          description: "Error Format Data"
        500:
          description: "Internal server error"
  /socket-event:
    post:
      operationId: "service.ServiceFacade.ServiceFacade.newSocketEvent"
      tags:
      - "socket-ajax"
      summary: create a new EventSocket, the service endpoint to be used is configured with enviorement var MS_EVENT_SOCKET 
      description: "New EventSocket"
      requestBody:
        description: EventSocket body
        required: true
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/EventSocket'
      responses:
        200:
          description: "Action creted Success"
        400:
          description: "Error Format Data"
        500:
          description: "Internal server error"
  /testroute:
    get:
      operationId: "service.ServiceFacade.ServiceFacade.testRoute"
      tags:
      - "mongo"
      summary: "list test route "
      description: "test task"
      parameters:
        - in: query
          description: Params 
          name: param
          schema:
            type: string
            example: '{ "limit": 50, "offset": 0,  "query": {}, "sort": [] }'
        
      responses:
        200:
          description: "Receive Data Ok"
        400:
          description: "Error Format Data"
        500:
          description: "Internal server error"
  /testtask:
    get:
      operationId: "service.ServiceFacade.ServiceFacade.testTask"
      tags:
      - "mongo"
      summary: "list test task "
      description: "test task"
      responses:
        200:
          description: "Receive Data Ok"
        400:
          description: "Error Format Data"
        500:
          description: "Internal server error"

  /testmongo/{collName}:
    get:
      operationId: "service.ServiceFacade.ServiceFacade.testMongo"
      tags:
      - "mongo"
      summary: Receive a collection name and test get data
      parameters:
        - in: path
          name: collName
          schema:
            type: string
          required: true
          description: name of collection to get
      responses:
        200:
          description: "Receive Data Ok"
        400:
          description: "Error Format Data"
        500:
          description: "Internal server error"
  /users:
    get:
      operationId: "service.ServiceFacade.ServiceFacade.list_users"
      tags:
      - "users"
      summary: "gets the first 50 users"
      description: "Allow to gets the first 50 users"
      responses:
        200:
          description: "successful operation"
          content:
            application/json:
              schema:
                type: "array"
                items:
                  $ref: "#/components/schemas/User"
        500:
          description: "Server internal error"
  /test_int/{int_value}:
    get:
      operationId: "service.ServiceFacade.ServiceFacade.valid_int"
      tags:
      - "input tets"
      summary: "test input"
      description: "check if given value is integer"
      parameters:
        - in: path
          name: int_value
          schema:
            type: integer
          required: true
          description: int value
      responses:
        200:
          description: "successful operation"
          content:
            application/json:
              schema:
                type: "string"
        404:
          description: "Order not found"
        500:
          description: "Server internal error"


  /stream:
    get:
      operationId: "service.ServiceFacade.ServiceFacade.stream_data"
      tags:
      - "stream"
      summary: "stream data"
      description: "getting data stream on server"
      responses:
        200:
          description: "successful operation"
          content:
            text/event-stream:
              schema:
                type: "string"
        404:
          description: "Order not found"
        500:
          description: "Server internal error"


  /file-upload:
    post:
      operationId: "service.ServiceFacade.ServiceFacade.file_upload"
      tags:
      - "file"
      summary: "upload file"
      description: "upload file"
      requestBody:
        content:
          multipart/form-data:
            schema:
              type: object
              properties:
                filename:
                  type: array
                  items:
                    type: string
                    format: binary
      responses:
        200:
          description: "successful operation"
        404:
          description: "Order not found"
        500:
          description: "Server internal error"

  /user/{id_user}:
    get:
      operationId: "service.ServiceFacade.ServiceFacade.get_user"
      tags:
      - "user"
      summary: Get user by id
      parameters:
        - in: path
          name: id_user
          schema:
            type: integer
          required: true
          description: id user required
      responses:
        200:
          description: "Receive Data Ok"
        400:
          description: "Error Format Data"
        500:
          description: "Internal server error"
components:
  schemas:
    SignalRequest:
      type: "object"
      required: 
        - "client"
        - "auth"
        - "signal"
      properties:
        client:
          type: "string"
          description: "client"
          example: "test"
        auth:
          type: "string"
          description: "auth"
          example: "Basic dGVzdDp0ZXN0"
        signal:
          $ref: '#/components/schemas/GPSSignal'
    
    Input:
      type: "object"
      properties:
        type:
          type: "string"
          description: "(Obligatorio) Nombre del input"
          example: "SENSOR_SOMNOLENCIA"
        inverted_logic:
          type: "boolean"
          description: "(Obligatorio) Indica si el campo posee lógica invertida"
          example: false
        value:
          type: "number"
          description: "(Obligatorio) Valor del input"
          example: 1
    GPSSignal:
      type: "object" 
      properties: 
        date_time:
          type: "string"
          description: "(Obligatorio) Fecha del registro, cumplir con formato Y-m-dTH-M-Sz"
          example: "2017-10-13T10:53:53-0400"
        num_plate:
          type: "string"
          description: "(Obligatorio) Patente Vehículo, sin guión ni espacios"
          example: "AABB11"
        gps_id:
          type: "string"
          description: "(Obligatorio) Identificador del GPS"
          example: "3344556677"
        lat:
          type: "number"
          description: "(Obligatorio) Coordenada del registro, separación decimal con punto"
          example: -33.000000
        lng:
          type: "number"
          description: "(Obligatorio) Coordenada del registro, separación decimal con punto"
          example: -70.000000
        altitude:
          type: "number"
          description: "(Obligatorio) Metros sobre el mar, separación decimal con punto"
          example: 0.00
        speed:
          type: "number"
          description: "(Obligatorio) Velocidad del Vehículo, separación decimal con punto"
          example: 60
        ignition:
          type: "integer"
          description: "(Obligatorio) Vehículo encendido o apagado, 0 ó1"
          example: 1
        nsat:
          type: "integer"
          description: "(Obligatorio) Numero de satélites"
          example: 4
        hdop:
          type: "number"
          description: "(Obligatorio) Precisión sobre horizonte de la tierra, desde 1 a 20"
          example: 1
        power:
          type: "integer"
          description: "(Obligatorio) Voltaje del dispositivo, desde 0 a 48"
          example: 48
        horometer:
          type: "integer"
          description: "(Obligatorio) Horas de encendido, cantidad en segundos"
          example: 3000
        odometer:
          type: "number"
          description: "(Obligatorio) Cantidad de kilometros, cantidad en metros"
          example: 5000
        panic:
          type: "integer"
          description: "(Obligatorio) Botón de pánico, 0 ó 1"
          example: 0
        batery:
          type: "number"
          description: "(Obligatorio) Voltaje batería GPS, separación decimal con punto"
          example: 5
        batery_power:
          type: "number"
          description: "(Obligatorio) Batería restante, separación decimal con punto"
          example: 2
        provider:
          type: "string"
          description: "(Obligatorio) Nombre proveedor GPS"
          example: "WAYPOINT"
        vehicle_type:
          type: "integer"
          description: "(Obligatorio) Tipo de vehículo, 1-Auto, 2-Suv, 3-Camioneta, 4-Moto, 5-Camión, 6-Remolque, 7-CasaRodante, 8-Grúa, 9-Maquinaría, 10-Bus, 11-Furgón, 12-Taxi, 13-Utilitario, 14-Camión 3/4, 15-Tractor."
          example: 5
        client:
          type: "string"
          description: "(Obligatorio) Nombre cliente"
          example: "CCU"
        provider_register:
          type: "string"
          description: "(Obligatorio) Fecha registro proveedor, cumplir con formato Y-m-dTH-M-Sz"
          example: "2017-10-13T10:53:53-0400"
        speed_unit:
          type: "integer"
          description: "(Obligatorio) Unidad de medida para velocidad, 0 = km/h 1 = millas/h"
          example: 0
        address:
          type: "string"
          description: "Dirección de la ubicación del GPS"
          example: "Miraflores 123, Santiago, Chile"
        carrier:
          type: "string"
          description: "Nombre transportista"
          example: "Transportes Gigante"
        cog:
          type: "number"
          description: "Indice de precisión GPS, separación decimal con punto"
          example: 0.00
        input1:
          $ref: "#/components/schemas/Input"
        input2:
          $ref: "#/components/schemas/Input"
        input3:
          $ref: "#/components/schemas/Input"
        input4:
          $ref: "#/components/schemas/Input"
        ibutton2:
          type: "string"
        ibutton3:
          type: "string"
        ibutton4:
          type: "string"
        adc:
          $ref: "#/components/schemas/Input"
        adc2:
          $ref: "#/components/schemas/Input"
        adc3:
          $ref: "#/components/schemas/Input"
        adc4:
          $ref: "#/components/schemas/Input"
        driverid:
          type: "string"
          description: "Identificador del conductor"
          example: "123456789"
        hdg:
          type: "number"
          description: "Rumbo triangulado, separación decimal con punto"
          example: 0.00
        dinc:
          type: "number"
          description: "Distancia incremental, separación decimal con punto"
          example: 0.00
        idgeocerca:
          type: "integer"
          description: "Identificador geocerca"
          example: 123
        estgeocerca:
          type: "integer"
          description: "Estado  geocerca, 0 ó 1"
          example: 0
        vgeocerca:
          type: "integer"
          description: "Velocidad geocerca"
          example: 30
    EventSocket:
      type: "object"
      required: 
      - "name"
      properties: 
        name:
          type: "string"
          description: "name"
          example: "user.connected.change"
        data:
          type: object
          additionalProperties: true
          description: "data"
          example: {"id":123,"name":"master"}
        notify:
          $ref: '#/components/schemas/Notify'
        token:
          type: "string"
          description: "token"
          example: 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoxOTAxMDIxLCJhY2NvdW50X2lkIjo3MDAxfQ.2scOC4xFQV6kZ0TqIjYimLht_kpEHfpIEv4D6-H7WK0'
    Notify:
      type: object
      required: 
      - "type"
      - "message"
      - "link"
      - "link_text"
      properties: 
        type:
          type: "string"
          enum: [event,message,alert,task]
          description: "event type"
          example: "event"
        message:
          type: "string"
          description: "message"
          example: "new event"
        link:
          type: "string"
          description: "link"
          example: "/"
        link_text:
          type: "string"
          description: "link_text"
          example: "see event"
    ListParam:
      type: "object"
      properties: 
        offset:
          type: "integer"
          description: "offset"
          example: 0
        limit:
          type: "integer"
          description: "limit"
          example: 50
        query:
          type: "object"
          additionalProperties: {}
          description: "query"
          example: {}
        sort:
          type: "object"
          additionalProperties: {}
          description: "sort"
          example: {}
    User:
      type: "object"
      required: 
      - "username"
      properties: 
        username:
          type: "string"
          description: "user name"
          example: "master"
        email:
          type: "string"
          description: "user mail"
          example: "dev@grupoalto.com"