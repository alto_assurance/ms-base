FROM python:3.7-slim-buster

# build args
ARG BRANCH_UTIL_LIB=master
ARG BRANCH_UTIL_MODEL=master

RUN apt-get update \
&& apt-get install -y \
    aufs-tools \
    curl \
    automake \
    build-essential \
    git \
    make \
    openssh-client \
    tzdata \
    default-libmysqlclient-dev \
&& echo "deb [signed-by=/usr/share/keyrings/cloud.google.gpg] https://packages.cloud.google.com/apt cloud-sdk main" | tee -a /etc/apt/sources.list.d/google-cloud-sdk.list \
&& apt-get install -y apt-transport-https ca-certificates gnupg \
&& curl https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key --keyring /usr/share/keyrings/cloud.google.gpg add - \
&& apt-get update \
&& apt-get install -y google-cloud-sdk \
&& rm -rf /var/lib/apt/lists/* \
&& pip install --upgrade pip \
&& pip install keyring \
&& pip install keyrings.google-artifactregistry-auth \
&& pip install gunicorn==19.9.0 \
&& apt-get remove -y curl \
&& rm -rf /var/lib/apt/lists/*

WORKDIR /app-run
COPY . /app-run
COPY root /root

RUN gcloud auth activate-service-account --key-file /app-run/.ssh/movup-dev-cicd.json

RUN pip install --no-cache-dir -r /app-run/requirements.txt
RUN bash /app-run/sh-bash/install_libs.sh

ENTRYPOINT gunicorn --conf /app-run/server_conf.py server:app --reload
